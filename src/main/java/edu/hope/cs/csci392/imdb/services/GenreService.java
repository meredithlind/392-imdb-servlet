package edu.hope.cs.csci392.imdb.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.hope.cs.csci392.imdb.Database;

@Path("genres")
public class GenreService {

	@GET
	@Produces(MediaType.APPLICATION_JSON)	
	public Response findAllRoles() {		
		Database db = Database.getInstance();
		List<String> genres = db.findGenres();
		return Response.ok(genres).build();		
	}
}
